import sys
import cv2
import numpy as np
from PyQt4 import QtGui, QtCore, Qt
from ui import Ui_MainWindow
from rexarm import Rexarm
import time
from video import Video
import math
import kinematics
from matplotlib import pyplot as plt
from matplotlib.pyplot import figure, title, imshow, show, close
import scipy
from scipy import signal
from scipy import misc
import multiple

""" Radians to/from  Degrees conversions """
D2R = 3.141592/180.0
PI = 3.141592
R2D = 180.0/3.141592
#H = 93#mm
#L = 100#mm
# = 110#mm
motorL = 115.0
armL = 100.0
tipL = 108.0

""" Pyxel Positions of image in GUI """
MIN_X = 310
MAX_X = 950

MIN_Y = 30
MAX_Y = 510
 
class Gui(QtGui.QMainWindow):
    """ 
    Main GUI Class
    It contains the main function and interfaces between 
    the GUI and functions
    """
    def __init__(self,parent=None):
        QtGui.QWidget.__init__(self,parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        """ Main Variables Using Other Classes"""
        self.rex = Rexarm()
        self.video = Video(cv2.VideoCapture(0))

        """ Other Variables """
        self.last_click = np.float32([0,0])
        self.waypoint_temp = []
        self.waypoint = []
        self.readyflag = 0;
        self.CVflag = 0;
        self.tempDef = []
        self.template_flag = 0
        self.real = []
        self.targetPos = []
        self.target_found = 0
        self.origWP = []
        self.playflag = 0
        self.oldtime = 0
        self.newtime = 0
        self.indexer = 0
        self.log = []
        self.vel = []
        self.tinit = 0

        """ Set GUI to track mouse """
        QtGui.QWidget.setMouseTracking(self,True)

        """     
        Video Function 
        Creates a timer and calls play() function 
        according to the given time delay (27mm) 
        """
        self._timer = QtCore.QTimer(self)
        self._timer.timeout.connect(self.play)
        self._timer.start(27)
        
        """ 
        LCM Arm Feedback
        Creates a timer to call LCM handler continuously
        No delay implemented. Reads all time 
        """  
        self._timer2 = QtCore.QTimer(self)
        self._timer2.timeout.connect(self.rex.get_feedback)
        self._timer2.start()
        """
        Timer
        Creat a timer to record the timestamp
        """
        self._timer3 = QtCore.QTimer(self)
        self._timer3.timeout.connect(self.timestamp)
        self._timer3.start(100)
        """ 
        Connect Sliders to Function
        TO DO: CONNECT THE OTHER 5 SLIDERS IMPLEMENTED IN THE GUI 
        """ 
        self.ui.sldrBase.valueChanged.connect(self.slider_change)
        self.ui.sldrShoulder.valueChanged.connect(self.slider_change)
        self.ui.sldrElbow.valueChanged.connect(self.slider_change)
        self.ui.sldrWrist.valueChanged.connect(self.slider_change)
        self.ui.sldrSpeed.valueChanged.connect(self.slider_change)
        self.ui.sldrMaxTorque.valueChanged.connect(self.slider_change)

        """ Commands the arm as the arm initialize to 0,0,0,0 angles """
        self.slider_change() 
        
        """ Connect Buttons to Functions """
        self.ui.btnLoadCameraCal.clicked.connect(self.load_camera_cal)
        self.ui.btnPerfAffineCal.clicked.connect(self.affine_cal)
        self.ui.btnTeachRepeat.clicked.connect(self.tr_initialize)
        self.ui.btnAddWaypoint.clicked.connect(self.tr_add_waypoint)
        self.ui.btnLoadPlan.clicked.connect(self.tr_load_plan)
        self.ui.btnSmoothPath.clicked.connect(self.tr_smooth_path)
        self.ui.btnPlayback.clicked.connect(self.tr_playback)
        self.ui.btnDefineTemplate.clicked.connect(self.def_template)
        self.ui.btnLocateTargets.clicked.connect(self.template_match)
        self.ui.btnExecutePath.clicked.connect(self.exec_path)



    def play(self):
        """ 
        Play Funtion
        Continuously called by GUI 
        """
        #rint self.video.aff_matrix
        """ Renders the Video Frame """
        try:
            self.video.captureNextFrame()
            p,q,l = self.video.currentFrame.shape
            if self.target_found == 1:
                for element in self.targetPos:
                    y = element[0]*2
                    x = element[1]*2
                    width = 1
                    size = 5
                    self.video.currentFrame[:,y-width:y+width,:] = [255,0,0]
                    self.video.currentFrame[x-width:x+width,:,:] = [255,0,0]
                    self.video.currentFrame[x-size:x+size,y-size:y+size,:] = [255,0,0]
            self.ui.videoFrame.setPixmap(
                self.video.convertFrame())
            self.ui.videoFrame.setScaledContents(True)
        except TypeError:
            print "No frame"
        '''theta = B
        phi = PI/2 - S - E/2
        R = 2*L*math.sin((PI-E)/2)
        ORIENT = (theta*R2D,(PI/2 - S - E - W)*R2D )
        POS = (R*math.cos(phi)*math.cos(theta), R*math.cos(phi)*math.sin(theta), H+R*math.sin(phi))
        print (R,phi*R2D)
        print "POS"+str(np.round(POS,decimals = 0))+ "  ORI" + str(ORIENT)'''
        
        """ 
        Update GUI Joint Coordinates Labels
        TO DO: include the other slider labels 
        """
        self.ui.rdoutBaseJC.setText(str(self.rex.joint_angles_fb[0]*R2D))
        self.ui.rdoutShoulderJC.setText(str(self.rex.joint_angles_fb[1]*R2D))
        self.ui.rdoutElbowJC.setText(str(self.rex.joint_angles_fb[2]*R2D))
        self.ui.rdoutWristJC.setText(str(self.rex.joint_angles_fb[3]*R2D))

        """ 
        Mouse position presentation in GUI
        TO DO: after getting affine calibration make the apprriate label
        to present the value of mouse position in world coordinates 
        """    
        x = QtGui.QWidget.mapFromGlobal(self,QtGui.QCursor.pos()).x()
        y = QtGui.QWidget.mapFromGlobal(self,QtGui.QCursor.pos()).y()

        if ((x < MIN_X) or (x > MAX_X) or (y < MIN_Y) or (y > MAX_Y)):
            self.ui.rdoutMousePixels.setText("(-,-)")
            self.ui.rdoutMouseWorld.setText("(-,-)")
        else:
            x = x - MIN_X
            y = y - MIN_Y
            self.ui.rdoutMousePixels.setText("(%.0f,%.0f)" % (x,y))
            if (self.video.aff_flag == 2):
                real=np.dot(self.video.aff_matrix,[[x],[y],[1]])
                #print real
                """ TO DO Here is where affine calibration must be used """
                self.ui.rdoutMouseWorld.setText("(%.0f,%.0f)" % (real[0],real[1]))
                '''
                z = time.time()*10%(2*PI*10)
                z = 20+15*np.sin(z)
                target = kinematics.searchIK(real[0],real[1],z)
                if target != None:
                    self.rex.joint_angles = [target[0],target[1],target[2],target[3]]
                    self.rex.max_torque = 0.4
                    self.rex.speed = [0.8, 0.2, 0.2, 0.2]
                    self.rex.cmd_publish()
                    '''
            else:
                self.ui.rdoutMouseWorld.setText("(-,-)")

        """ 
        Updates status label when rexarm playback is been executed.
        This will be extended to includ eother appropriate messages
        """ 
        if(self.rex.plan_status == 1):
            self.ui.rdoutStatus.setText("Playing Back - Waypoint %d"
                                    %(self.rex.wpt_number + 1))
            ############################ WOZAIZHE
        #self.forwardK()

        """Forward Kinematics"""
        FK = self.forwardK()
        self.ui.rdoutX.setText(str(FK[0]))
        self.ui.rdoutY.setText(str(FK[1]))
        self.ui.rdoutZ.setText(str(FK[2]))
        self.ui.rdoutT.setText(str(FK[3]))

        

    def slider_change(self):
        """ 
        Function to change the slider labels when sliders are moved
        and to command the arm to the given position 
        TO DO: Implement for the other sliders
        """
        self.ui.rdoutBase.setText(str(self.ui.sldrBase.value()))
        self.ui.rdoutShoulder.setText(str(self.ui.sldrShoulder.value()))
        self.ui.rdoutElbow.setText(str(self.ui.sldrElbow.value()))
        self.ui.rdoutWrist.setText(str(self.ui.sldrWrist.value()))
        self.ui.rdoutTorq.setText(str(self.ui.sldrMaxTorque.value()) + "%")
        self.ui.rdoutSpeed.setText(str(self.ui.sldrSpeed.value()) + "%")
        self.rex.max_torque = self.ui.sldrMaxTorque.value()/100.0
        self.rex.speed = [self.ui.sldrSpeed.value()/100.0,self.ui.sldrSpeed.value()/100.0,self.ui.sldrSpeed.value()/100.0,self.ui.sldrSpeed.value()/100.0]
        self.rex.joint_angles = [self.ui.sldrBase.value()*D2R,\
                                 self.ui.sldrShoulder.value()*D2R,\
                                 self.ui.sldrElbow.value()*D2R,\
                                 self.ui.sldrWrist.value()*D2R]
        self.rex.cmd_publish()










    def mousePressEvent(self, QMouseEvent):
        """ 
        Function used to record mouse click positions for 
        affine calibration 
        """
 
        """ Get mouse posiiton """
        x = QMouseEvent.x()
        y = QMouseEvent.y()

        """ If mouse position is not over the camera image ignore """
        if ((x < MIN_X) or (x > MAX_X) or (y < MIN_Y) or (y > MAX_Y)): return

        """ Change coordinates to image axis """
        self.last_click[0] = x - MIN_X
        self.last_click[1] = y - MIN_Y


        """ If affine calibration is been performed """
        if (self.video.aff_flag == 1):
            """ Save last mouse coordinate """
            self.video.mouse_coord[self.video.mouse_click_id] = [(x-MIN_X),
                                                                 (y-MIN_Y)]

            """ Update the number of used poitns for calibration """
            self.video.mouse_click_id += 1

            """ Update status label text """
            self.ui.rdoutStatus.setText("Affine Calibration: Click point %d" 
                                      %(self.video.mouse_click_id + 1))

            """ 
            If the number of click is equal to the expected number of points
            computes the affine calibration.
            TO DO: Change this code to use you programmed affine calibration
            and NOT openCV pre-programmed function as it is done now.
            """
            if(self.video.mouse_click_id == self.video.aff_npoints):
                """ 
                Update status of calibration flag and number of mouse
                clicks
                """
                self.video.aff_flag = 2
                self.video.mouse_click_id = 0
                
                """ Perform affine calibration with OpenCV """
                """self.video.aff_matrix = cv2.getAffineTransform(
                                        self.video.mouse_coord,
                                        self.video.real_coord)
                self.video.aff_matrix=np.vstack((self.video.aff_matrix,[0,0,1]))"""
                self.video.aff_matrix=self.getTransform()
                np.savetxt("aff_matrix.txt",self.video.aff_matrix)

                """ Updates Status Label to inform calibration is done """ 
                self.ui.rdoutStatus.setText("Waiting for input")

                """ 
                Uncomment to gether affine calibration matrix numbers 
                on terminal
                """ 
                print self.video.aff_matrix

        """Capture the template"""
        if self.CVflag == 1:
            self.tempDef.append([self.last_click[0],self.last_click[1]])
            self.CVflag = 0
            try:
                self.video.captureNextFrame()
                self.ui.videoFrame.setPixmap(
                    self.video.convertFrame())
                self.ui.videoFrame.setScaledContents(True)
            except TypeError:
                print "No frame"
            k = 2
            image = np.copy(self.video.currentFrame[:,:,:])
            image = scipy.misc.imresize(image,[480/k,640/k], interp='bilinear', mode=None)
            r,g,b = image[:,:,0],image[:,:,1],image[:,:,2]
            image = image[:,:,0]#0.2989*r+0.5870*g+0.1130*b
            print image.shape
            boundx1 = 200/2/k#*i2v[0]
            boundy1 = 400/2/k#*i2v[0]
            boundx2 = 800/2/k#*i2v[1]
            boundy2 = 1000/2/k#*i2v[1]

            sample = image[boundx1:boundx2,boundy1:boundy2]
            sample = sample# - sample.mean()
            ty1 = self.tempDef[0][0]/k
            tx1 = self.tempDef[0][1]/k
            ty2 = self.tempDef[1][0]/k
            tx2 = self.tempDef[1][1]/k
            if tx1>tx2:
                temp = tx1
                tx1 = tx2
                tx2 = temp
            if ty1>ty2:
                temp = ty1
                ty1 = ty2
                ty2 = temp
            template = np.copy(image[tx1:tx2,ty1:ty2])
            template = template# - template.mean()
            np.savetxt('TMP.txt',template)
            print "template saved"
            self.template_flag = 1
        if self.CVflag == 2:
            self.CVflag = 1
            self.tempDef.append([self.last_click[0],self.last_click[1]])

    def getTransform(self):
        row=2*len(self.video.mouse_coord)
        A=np.zeros((row,6))
        j=0
        for i in range(row):
            if i%2==0:
                A[i][0]=self.video.mouse_coord[j][0]
                A[i][1]=self.video.mouse_coord[j][1]
                A[i][2]=1
            else:
                A[i][3]=self.video.mouse_coord[j][0]
                A[i][4]=self.video.mouse_coord[j][1]
                A[i][5]=1
                j=j+1

        Atrans=np.transpose(A)
        Amat=np.dot(Atrans,A)
        AInv=np.linalg.inv(Amat)
        b=[]
        for i in range(row/2):
            b.append(self.video.real_coord[i][0])
            b.append(self.video.real_coord[i][1])
        res=np.dot(np.dot(AInv,Atrans),b)
        result = [[res[0],res[1],res[2]],
                  [res[3],res[4],res[5]],
                  [0,0,1]]
        return result 

    def affine_cal(self):
        """ 
        Function called when affine calibration button is called.
        Note it only chnage the flag to record the next mouse clicks
        and updates the status text label 
        """
        self.video.aff_flag = 1 
        print "Affine calibration start"
        self.ui.rdoutStatus.setText("Affine Calibration: Click point %d" 
                                    %(self.video.mouse_click_id + 1))


    def load_camera_cal(self):
        self.video.loadCalibration()
        self.video.aff_matrix =  np.loadtxt("aff_matrix.txt")
        self.video.aff_flag = 2
        print "Load Camera Cal"

    def tr_initialize(self):
        self.waypoint = []
        self.waypoint_temp = []
        self.readyflag = 0
        print "Teach and Repeat"
        self.rex.joint_angles = [-1.8,1,0,0]
        self.rex.max_torque = 0.4
        self.rex.speed = [0.8, 0.2, 0.2, 0.2]
        self.rex.cmd_publish()
        time.sleep(1)
        self.rex.max_torque = 0
        self.rex.cmd_publish()
        print "Ready, please teach me"


    def tr_add_waypoint(self):
        self.readyflag = 0
        print "Add Waypoint"
        B = self.rex.joint_angles_fb[0]
        S = self.rex.joint_angles_fb[1]
        E = self.rex.joint_angles_fb[2]
        W = self.rex.joint_angles_fb[3]
        waypoint = (B,S,E,W)
        self.waypoint_temp.append(waypoint)
        print self.waypoint_temp
        np.savetxt('WP.txt',self.waypoint_temp)

    def tr_load_plan(self):
        print "Loading.."
        self.waypoint = np.loadtxt('WP.txt')
        self.origWP = np.loadtxt('WP.txt')
        self.readyflag = 1
        print self.waypoint
        print "ready to go!"

    def tr_smooth_path(self):
        version=1
        if version==1:
            print "Smooth Path"
            if self.readyflag ==0:
                print "Please load waypoints!"
                return
            target = []
            extra = []
            windowOneside = 2
            windowSize = 1 + 2 * windowOneside
            for i in range(0,len(self.waypoint)):
                if i % 2:#odd index is extra points
                    diff_pre = self.waypoint[i] - self.waypoint[i - 1]
                    diff_pro = self.waypoint[i + 1] - self.waypoint[i]
                    extra.append(self.waypoint[i-1] + 0.25*diff_pre)
                    extra.append(self.waypoint[i-1] + 0.5*diff_pre)
                    extra.append(self.waypoint[i-1] + 0.75*diff_pre)
                    extra.append(self.waypoint[i])
                    extra.append(self.waypoint[i] + 0.25*diff_pro)
                    extra.append(self.waypoint[i] + 0.5*diff_pro)
                    extra.append(self.waypoint[i] + 0.75*diff_pro)
                else:#even index is target
                    target.append(self.waypoint[i])
            target_big = []
            for element in target:
                for j in range (0,windowSize):
                    target_big.append(element)
            length = len(target_big) + len(extra)
            raw = []
            index_e = 0
            index_t = 0
            for i in range(0,length):
                if i % (windowSize + 7) +1 > windowSize:
                    raw.append(extra[index_e])
                    index_e += 1
                else:
                    raw.append(target_big[index_t])
                    index_t +=1
            smoothed = []
            for i in range(0+windowOneside,length-windowOneside):
                running = 0
                for j in range(i - windowOneside, i + windowOneside + 1):
                    running += raw[j]
                smoothed.append(running/windowSize)
            smoothed = np.array(smoothed)
            self.waypoint = []
            self.waypoint.append([0,0,0,0])
            for element in smoothed:
                self.waypoint.append(element)
            self.waypoint.append([0,0,0,0])
            self.waypoint = np.array(self.waypoint)
            print self.waypoint
        else:
            waypoint_new = []
            waypoint_new.append([0,0,0,0])
            for i in range (0,len(self.waypoint)):
                waypoint_new.append(self.waypoint[i]+[0.0,-20.0*D2R,0.0,20.0*D2R])
                waypoint_new.append(self.waypoint[i])
                waypoint_new.append(self.waypoint[i]+[0.0,-20.0*D2R,0.0,20.0*D2R])
            waypoint_new.append([0,0,0,0])
            self.waypoint = np.array(waypoint_new)
    def extraPoint(self, p):
        B = p[0]
        S = p[1]-15.0*D2R
        E = p[2]
        W = p[3]+15.0*D2R
        return [B,S,E,W]

    def timestamp(self):
        while 1:
            self.newtime = time.time()
            elapse = self.newtime-self.oldtime
            if elapse < 0.00001:
                return
            if self.playflag == 0:
                return

            if self.indexer == len(self.waypoint):
                np.savetxt("pos.log",self.log)
                np.savetxt("vel.log",self.vel)
                self.indexer = 0
                self.playflag = 0
                return
            cnt = 0 
            
            a = len(self.waypoint)
            b = 0
            element = self.waypoint[self.indexer]
            flag = 0
            for angles in self.waypoint:
                if np.round(element[0],decimals = 3) == np.round(angles[0],decimals = 3) and np.round(element[1],decimals = 3) == np.round(angles[1],decimals = 3) and np.round(element[2],decimals = 3) == np.round(angles[2],decimals = 3) and np.round(element[3],decimals = 3) == np.round(angles[3],decimals = 3):
                    flag = 1
            self.rex.joint_angles = element
            self.rex.max_torque = 0.5
            spd = 0.2
            self.rex.speed = [spd,spd,spd,spd]
            self.rex.cmd_publish()
            error = abs(self.rex.joint_angles_fb - self.rex.joint_angles)
            B = self.rex.joint_angles_fb[0]
            S = self.rex.joint_angles_fb[1]
            E = self.rex.joint_angles_fb[2]
            W = self.rex.joint_angles_fb[3]
            vB = self.rex.speed_fb[0]
            vS = self.rex.speed_fb[1]
            vE = self.rex.speed_fb[2]
            vW = self.rex.speed_fb[3]

            margin = 0.08
            if error[0]>=margin or error[1]>=margin or error[2]>=margin or error[3]>=margin:
                self.rex.get_feedback()
                error = abs(self.rex.joint_angles_fb - self.rex.joint_angles)
                B = self.rex.joint_angles_fb[0]
                S = self.rex.joint_angles_fb[1]
                E = self.rex.joint_angles_fb[2]
                W = self.rex.joint_angles_fb[3]
                vB = self.rex.speed_fb[0]
                vS = self.rex.speed_fb[1]
                vE = self.rex.speed_fb[2]
                vW = self.rex.speed_fb[3]

                self.log.append([B,S,E,W,time.time()-self.tinit])
                self.vel.append([vB,vS,vE,vW,time.time()-self.tinit])

                #for j in range(0,4):
                #    self.rex.speed[j] = (error[j] / PI)*0.4
                #    self.rex.speed[j] = 0.2
                self.rex.cmd_publish()
                print "I published"
            else:
                self.log.append([B,S,E,W,-time.time()+self.tinit])
                self.indexer = self.indexer + 1
            if flag == 1:
                #log.append([B,S,E,W,-time.time()+tinit])
                B = self.waypoint[cnt][0]
                S = self.waypoint[cnt][1]
                E = self.waypoint[cnt][2]
                W = self.waypoint[cnt][3]
                #log.append([B,S,E,W,-time.time()+tinit])
                cnt = cnt+1
            b = b+1
            if b == a:
                self.playflag = 0
            
            self.oldtime = self.newtime
            


    def tr_playback(self):
        self.playback=1
        if self.readyflag == 0:
            print "Please load waypoints!"
            return
        print "Playback"
        if self.waypoint.size < 5:
            print "Kidding me?"
            return
        self.origWP = np.loadtxt('WP.txt')
        self.playflag = 1
        self.tinit = time.time()
        self.oldtime = time.time()
        self.indexer = 0
        self.log = []
        self.vel = []

                #print error
            #time.
            #time.sleep(1/3.0)   
    def forwardK(self):
        B = self.rex.joint_angles_fb[0]
        S = self.rex.joint_angles_fb[1]
        E = self.rex.joint_angles_fb[2]
        W = self.rex.joint_angles_fb[3]
        theta = abs(S+E+W) - PI/2
        theta = theta * R2D
        result = kinematics.forwardK([0,0,0],[B,S,E,W])
        np.append(result,[theta])
        #result = np.round(result,decimals=2)
        result = [result[0],result[1],result[2],theta]
        #print np.round(result,decimals = 0)
        return result

    def def_template(self):
        if (self.video.aff_flag != 2):
            print "Please do affine calibration first!"
            return
        print "Define Template"
             
        '''print "follow fun path"
        self.readyflag = 1
        self.waypoint = np.round(kinematics.funPath(50,[100,-100,60]),decimals = 2)
        np.savetxt('WP.txt',self.waypoint)
        print self.waypoint
        '''
        print self.last_click
        self.CVflag = 2
        self.tempDef = []
        self.target_found = 0
        
    def template_match(self):
        if (self.video.aff_flag != 2):
            print "Please do affine calibration first!"
            return
        if (self.template_flag == 0):
            print "Please define template first"
            return
        print "Template Match"
        try:
            self.video.captureNextFrame()
            self.ui.videoFrame.setPixmap(
                self.video.convertFrame())
            self.ui.videoFrame.setScaledContents(True)
        except TypeError:
            print "No frame"
        k = 2
        image = np.copy(self.video.currentFrame[:,:,:])
        image = scipy.misc.imresize(image,[480/k,640/k], interp='bilinear', mode=None)
        r,g,b = image[:,:,0],image[:,:,1],image[:,:,2]
        image = image[:,:,0]#0.2989*r+0.5870*g+0.1130*b
        print image.shape
        boundx1 = 200/2/k#*i2v[0]
        boundy1 = 400/2/k#*i2v[0]
        boundx2 = 800/2/k#*i2v[1]
        boundy2 = 1000/2/k#*i2v[1]

        sample = image[boundx1:boundx2,boundy1:boundy2]
        sample = sample# - sample.mean()
        ty1 = self.tempDef[0][0]/k
        tx1 = self.tempDef[0][1]/k
        ty2 = self.tempDef[1][0]/k
        tx2 = self.tempDef[1][1]/k
        if tx1>tx2:
            temp = tx1
            tx1 = tx2
            tx2 = temp
        if ty1>ty2:
            temp = ty1
            ty1 = ty2
            ty2 = temp
        '''template = np.copy(image[tx1:tx2,ty1:ty2])
        template = template# - template.mean()'''
        template = np.loadtxt("TMP.txt")
        #corr = signal.correlate2d(sample, template, boundary='symm', mode='same')
        x, y, corr = self.myCorr(sample,template)#np.unravel_index(np.argmax(corr), corr.shape) # find the match
        #print (x, y)
        self.targetPos = multiple.findTargets(corr,boundx1,boundy1,tx2-tx1,ty2-ty1,k,1.5)
        print self.targetPos
        fig, (F1,F2,F3) = plt.subplots(1, 3)
        F1.imshow(sample, cmap='gray')
        F2.imshow(template, cmap='gray')
        F3.imshow(corr,cmap='gray')
        fig.show()
        self.waypoint = []
        for element in self.targetPos:
            temp = np.dot(self.video.aff_matrix,[[element[0]],[element[1]],[1]])
            target = kinematics.searchIK(temp[0],temp[1],5)
            if target != None:
                self.waypoint.append([target[0]+PI,target[1],target[2],target[3]])     
        #self.waypoint.append([0,0,0,0])
        self.waypoint = np.array(self.waypoint)
        self.waypoint.view('i8,i8,i8,i8').sort(order=['f0'], axis=0)        
        waypointkinematics = []
        waypointkinematics.append([0,0,armL*2+motorL+tipL-1])
        print self.waypoint
        for element in self.waypoint:
            element[0] = element[0] - PI
            jointToworld = kinematics.joint2world(element[0],element[1],element[2],element[3])
            waypointkinematics.append([jointToworld[0],jointToworld[1],jointToworld[2]])
        waypointkinematics.append([0,0,armL*2+motorL+tipL-1])
        waypointkinematics = np.array(waypointkinematics)
        print "waypointkinematics" 
        print waypointkinematics
        #tempworld=[]
        tempjoint=[]
        for i in range (0,len(waypointkinematics)-1):
            airPoint = waypointkinematics[i]/2+waypointkinematics[i+1]/2
            #tempworld.append(waypointkinematics[i])
            #tempworld.append(airPoint)
            tempjoint.append(kinematics.searchIK(waypointkinematics[i][0],waypointkinematics[i][1],waypointkinematics[i][2]))
            tempjoint.append(kinematics.searchIK(airPoint[0],airPoint[1],180))
        #tempworld.append(waypointkinematics[-1])
        tempjoint.append(kinematics.searchIK(waypointkinematics[-1][0],waypointkinematics[-1][1],waypointkinematics[-1][2]))
        self.waypoint = np.array(tempjoint)
        self.origWP = np.copy(self.waypoint)
        print "new waypoint" + str(self.waypoint)
        #np.savetxt("correlation.fuck",corr)
        #targy = (boundx1+x)*k
        #targx = (boundy1+y)*k
        #print (targx,targy)
        #self.real = np.dot(self.video.aff_matrix,[[targx],[targy],[1]])
        ###Don't needed any more #self.targetPos = [targx,targy]
        self.target_found = 1
        
    def myCorr(self, image, template):
        result = []
        m,n = image.shape # m,n are the #of rows and columns
        p,q = template.shape
        for i in range(0,m-p):
            result_row = []
            for j in range(0,n-q):
                x1 = i
                x2 = i+p
                y1 = j
                y2 = j+q
                temp_image = image[i:i+p,j:j+q]
                diff = abs(template-temp_image)
                #print diff
                result_row.append(diff.mean())
            result.append(result_row)
        result = np.array(result)
        x,y = np.unravel_index(np.argmin(result),result.shape)
        y = y + np.round(q/2)
        x = x + np.round(p/2)
        return x,y,result

    def exec_path(self):
        if self.target_found == 0:
            print "Please respect me ok?"
            return
        self.readyflag = 1
        print "Execute Path"
        print "Raw target waypoints found by CV:"
        print self.waypoint
 
def main():
    app = QtGui.QApplication(sys.argv)
    ex = Gui()
    ex.show()
    sys.exit(app.exec_())
 
if __name__ == '__main__':
    main()
