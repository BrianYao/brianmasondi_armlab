import numpy as np
from matplotlib import pyplot as plt
from matplotlib.pyplot import figure, title, imshow, show,close
from scipy import signal
from scipy import misc

def findTargets(cor,boundx1,boundy1,mythx,mythy,k,sigma):
    row,col = cor.shape;
    ave = cor[:,:].mean()
    low = cor[:,:].min()
    high = cor[:,:].max()
    rag = high-low;
    std = cor[:,:].std()
    thresh = low+std*sigma
    
    for i in range(0,row):
        for j in range(0,col):
            if cor[i][j]>thresh:
                cor[i][j] = 0
            else:
                cor[i][j] = 1
    kernel = [[1,1,1],[1,0,1],[1,1,1]]
    for i in range(1,row-1):
        for j in range(1,col-1):
            temp = cor[i-1:i+2,j-1:j+2]
            mult = temp*kernel
            exist = mult.max()
            if exist:
                cor[i][j] = 0
    result = []
    for i in range(1,row):
        for j in range(1,col):
            if cor[i][j] == 1:
                targx = (boundy1+j)*k    +mythy - 1#the weird offset error..have no idea
                targy = (boundx1+i)*k    +mythx -1#+mythrow
                result.append([targx,targy])
                
    return np.array(result)

    
if __name__ == "__main__":
    '''close(1)
    cor = np.loadtxt("correlation.txt")
    fig = figure(1)
    plt.imshow(cor,cmap = 'gray')
    fig.show()
    result = findTargets(cor)
    print result'''
    a = np.array([[-8.2,2,3,6],[-4.4,5.2,6.6,3],[-0.4,0,1,6]])
    a.view('i8,i8,i8,i8').sort(order=['f0'], axis=0)
    print a