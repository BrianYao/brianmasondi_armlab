import cv2
import numpy as np
from PyQt4 import QtGui, QtCore, Qt

class Video():
    def __init__(self,capture):
        self.capture = capture
        self.capture.set(3, 1280)
        self.capture.set(4, 960)
        self.currentFrame=np.array([])
    	self.notCal=1
    	self.camera_matrix=0
    	self.dist_coefs=0

        """ 
        Affine Calibration Variables 
        Currently only takes three points: center of arm and two adjacent 
        corners of the base board
        Note that OpenCV requires float32 arrays
        """
        self.aff_npoints = 5                                    # Change!
        self.real_coord = np.float32([[0., 0.], [302.,299], [295,-297],[-305,-290],[-300,307]])
        self.mouse_coord = np.float32([[0.0, 0.0],[0.0, 0.0],[0.0, 0.0],[0.0, 0.0],[0.0, 0.0]])      
        self.mouse_click_id = 0
        self.aff_flag = 0
        self.aff_matrix = np.float32((2,3))
    
    def captureNextFrame(self):
        """                      
        Capture frame, convert to RGB, and return opencv image      
        """
        ret, frame=self.capture.read()
        if(ret==True) and self.notCal:
            self.currentFrame=cv2.cvtColor(frame, cv2.COLOR_BAYER_GB2BGR)
        elif ret==True:
            w = 1280
            h = 960
    	    new_camera_matrix, roi = cv2.getOptimalNewCameraMatrix(self.camera_matrix,self.dist_coefs,(w,h),1,(w,h))
    	    rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BAYER_GB2BGR)
            undistorted = cv2.undistort(rgb_frame, self.camera_matrix, self.dist_coefs, None, new_camera_matrix)
            self.currentFrame=undistorted
    def convertFrame(self):
        """ Converts frame to format suitable for QtGui  """
        try:
            height,width=self.currentFrame.shape[:2]
            img=QtGui.QImage(self.currentFrame,
                              width,
                              height,
                              QtGui.QImage.Format_RGB888)
            img=QtGui.QPixmap.fromImage(img)
            self.previousFrame = self.currentFrame
            return img
        except:
            return None

    def loadCalibration(self):

        self.camera_matrix=np.loadtxt('camera_matrix.txt')
    	self.dist_coefs=np.loadtxt('dis_coefs.txt')
        print "work"
    	self.notCal=0

"""
        Load calibration from file and applies to the image
        Look at camera_cal.py final lines to see how that is done
        """


