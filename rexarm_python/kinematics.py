import numpy as np
import csv
import string
import sys
from matplotlib.pyplot import *
import time
import math


PI = 3.141592
D2R = PI/180.0
R2D = 180/PI
motorL = 115.0
armL = 100.0
tipL = 108.0
default_phi = PI/2

def Acalc(theta,d,a,alpha):
    RTz = np.array([[np.cos(theta),-np.sin(theta),0,0],\
                   [np.sin(theta),np.cos(theta),0,0],\
                   [0,0,1,d],[0,0,0,1]])
    TRx = np.array([[1,0,0,a],\
                    [0,np.cos(alpha),-np.sin(alpha),0],\
                    [0,np.sin(alpha),np.cos(alpha),0],\
                    [0,0,0,1]])
    return np.dot(RTz,TRx)

def joint2world(B,S,E,W):
    B = B + PI
    S = S + PI/2
    p = np.array([[0],[0],[0],[1]])
    A1 = Acalc(B,motorL,0,90*D2R)
    A2 = Acalc(S,0,armL,0)
    A3 = Acalc(E,0,armL,0)
    A4 = Acalc(W,0,tipL,0)
    temp1 = np.dot(A1,A2)
    temp2 = np.dot(A3,A4)
    return np.dot(np.dot(temp1,temp2),p)

def forwardK(pos,cmd):
    B = cmd[0]
    S = cmd[1]
    E = cmd[2]
    W = cmd[3]
    pos.append(0)
    pos = np.transpose(pos)
    origin = np.array([0,0,0,1])
    origin = np.transpose(origin)
    result = joint2world(B,S,E,W)
    return np.delete(result,-1)

def searchIK(*inputs):
    x = inputs[0]
    y = inputs[1]
    z = inputs[2]
    result = None
    for phi in range(90,-91,-1):
        if result == None:
            result = inverseK(x,y,z,phi*D2R)
        else:
            return result
    result = None
    for phi in range(90,180):
        if result == None:
            result = inverseK(x,y,z,phi*D2R)
        else:
            return result
    return result

def inverseK(x,y,z,phi):    
    if (x**2+y**2)**0.5 - armL*2 - tipL > 0 or z<0:
        #print "Tip unreachable whatsoever"
        return None
    B = math.atan2(y,x)
    if B > PI:
        B = B - 2*PI
    rg = (x**2+y**2)**0.5
    rg_prime = rg - np.cos(phi)*tipL
    zg_prime = z + np.sin(phi)*tipL
    delta_z = zg_prime - motorL
    pseudo_x = rg_prime
    pseudo_y = delta_z
    stretch = (delta_z**2+rg_prime**2)**0.5 - 0.00000000001
    if stretch > 2*armL or zg_prime < 0:
        #print "Elbow unreachable with specified phi"
        return None
    E = np.arccos((stretch/2)/armL)*2
    pseudo_SW = math.atan2(pseudo_y,pseudo_x)
    pseudo_SE = pseudo_SW + E/2
    S = PI/2 - pseudo_SE
    W = PI/2 + phi - E -S
    if S>121*D2R or S<-125*R2D or E>124*D2R or E<-122*D2R or W>126*D2R or W<-128*D2R:
        return None
    #print [pseudo_x,zg_prime]
    #print [B*R2D,S*R2D,E*R2D,W*R2D]
    return np.array([B,S,E,W])

def funPath(scale,offset):
    pos = []
    scale = scale / 2
    for t in range (0,100):
        t = t/50.0
        x = scale * np.sin(PI*2*t)+offset[0]
        y = scale * np.cos(PI*3*t)+offset[1]
        z = 10*np.sin(PI*t)+offset[2]
        h = 1
        pos.append([x,y,z,h])
    pos = np.array(pos)
    rotation = np.array([[np.cos(1),0,-np.sin(1),0],[0,1,0,0],[np.sin(1),0,np.cos(1),0],[0,0,0,1]])
    pos = np.dot(rotation,np.transpose(pos))
    wayPoint = []
    temp3 = []
    for i in range (0,len(pos[0])):
        x = pos[0][i]
        y = pos[1][i]
        z = pos[2][i]
        a = searchIK(x,y,z)
        if a != None:
            wayPoint.append(a)
        else:
            print "Position " + str([x,y,z]) + " unreachable!"
    return np.array(wayPoint)


if __name__ == "__main__":
    #print searchIK(0,-armL*2-tipL,motorL)
    #funPath(50,[50,-50,0])
    #a = np.array([1,2,3])
    #b = np.array([[1,2,3],[4,5,6]])
    #if a in b:
    #    print "yes"
    #print math.atan2(-1,1)*R2D
    print forwardK([0,0,0,0],[0,0,0,0])
